$(document).ready(function () {

    $('.header__slider-box').slick({
        arrows: false,
        dots: true
    })


    $('.gallery__box').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        }
    });

    $('.gallery__video').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });


    let links = $('.header__link');

    $('.header__link').hover(function () {
        $(this).find('.header__link-img').toggleClass('active')
    })
});